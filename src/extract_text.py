import tika
import re
from tika import parser
tika.initVM()


def extract_text_from_resume(file_path):
    
    results = parser.from_file(filename=file_path)
    # document_text = results['content']
    
    # document_text = remove_metadata(document_text)
    # return document_text

def remove_metadata(text):
    # Define regular expressions to match metadata and unwanted generic strings
    # regex_list = []
    regex_list = [
        r'^Title:.*$',
        r'^Author:.*$',
        r'^CreationDate:.*$',
        r'^ModDate:.*$',
        r'^Producer:.*$',
        r'^Keywords:.*$',
        r'^Subject:.*$',
        r'^Content-Type:.*$',
        r'^Resume.*$',
        r'^CV.*$',
    ]
    
    # Remove matching patterns from the text
    for regex in regex_list:
        text = re.sub(regex, '', text, flags=re.MULTILINE)
    
    return text.strip()
