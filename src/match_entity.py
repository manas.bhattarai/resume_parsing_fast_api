from dateutil import relativedelta
from datetime import datetime
import re
import pandas as pd
from nltk.corpus import stopwords
import nltk
from nltk.stem import WordNetLemmatizer
nltk.download('wordnet')
nltk.download('stopwords')

def extract_name(nlp, text):
    # Process the text with spaCy
    doc = nlp(text)

    # Initialize an empty dictionary to store the extracted names
    names = {
        'first_name': '',
        'middle_name': '',
        'last_name': ''
    }

    # Look for entities in the text that are labeled as a person (PERSON)
    for ent in doc.ents:
        if ent.label_ == 'PERSON':
            # Split the entity text into tokens and determine the first, middle, and last names
            tokens = ent.text.split()
            if len(tokens) == 1:
                # If there is only one token, assume it is the first name
                names['first_name'] = tokens[0]
            elif len(tokens) == 2:
                # If there are two tokens, assume the first is the first name and the second is the last name
                names['first_name'] = tokens[0]
                names['last_name'] = tokens[1]
            elif len(tokens) == 3:
                # If there are three tokens, assume the first is the first name, the second is the middle name, and the third is the last name
                names['first_name'] = tokens[0]
                names['middle_name'] = tokens[1]
                names['last_name'] = tokens[2]
            else:
                # If there are more than three tokens, assume the last three are the middle and last name
                names['first_name'] = tokens[0]
                names['middle_name'] = ' '.join(tokens[1:-1])
                names['last_name'] = tokens[-1]

            break

    return names


def extract_contact_info(text):
    # Extract phone number using regular expression
    phone_regex = r'\b(?:\d[ -.]*){9,}\b'
    phone_number = re.findall(phone_regex, text)

    # Extract email address using regular expression
    email_regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    email = re.findall(email_regex, text)

    contact_info = {
        'phone': phone_number,
        'email': email
    }

    return contact_info


def extract_skills(nlp, resume_text):
    nlp_text = nlp(resume_text)

    # removing stop words and implementing word tokenization
    tokens = [token.text for token in nlp_text if not token.is_stop]

    # reading the csv file
    data = pd.read_csv("../data/skills.csv")
    
    

    # extract values
    skills = list(data.columns.values)

    skillset = []

    # check for one-grams (example: python)
    for token in tokens:
        if token.lower() in skills:
            skillset.append(token)

    # check for bi-grams and tri-grams (example: machine learning)
    for chunk in nlp_text.noun_chunks:
        chunk_text = chunk.text.lower().strip()
        if chunk_text in skills:
            skillset.append(chunk_text)

    return [i.capitalize() for i in set([i.lower() for i in skillset])]


def extract_education(nlp, resume_text):
    nlp_text = nlp(resume_text)

    # Split document into sentences
    nlp_text = [token.sent.text.strip() for token in nlp_text]

    # Grad all general stop words
    STOPWORDS = set(stopwords.words('english'))

    # Education Degrees
    EDUCATION = [
        'BE', 'B.E.', 'B.E', 'BS', 'B.S',
        'ME', 'M.E', 'M.E.', 'MS', 'M.S',
        'Bachelors in Electronics and Communication Engineering',
        'BTECH', 'B.TECH', 'M.TECH', 'MTECH',
        'SSC', 'HSC', 'CBSE', 'ICSE', 'X', 'XII',
        'BCA', 'CSIT', 'BBS', 'BBA', 'CA', 'BSW', '+2',
        'NEB'
    ]

    edu = {}
    # Extract education degree
    for index, text in enumerate(nlp_text):
        for tex in text.split():
            # Replace all special symbols
            tex = re.sub(r'[?|$|.|!|,]', r'', tex)
            if tex.upper() in EDUCATION and tex not in STOPWORDS:
                edu[tex] = text + nlp_text[index + 1]

    # Extract year
    education = []
    for key in edu.keys():
        year = re.search(re.compile(r'(((20|19)(\d{2})))'), edu[key])
        if year:
            education.append((key, ''.join(year[0])))
        else:
            education.append(key)
    return education


def extract_experience(resume_text):
    '''
    Helper function to extract experience from resume text
    :param resume_text: Plain resume text
    :return: list of experience
    '''
    wordnet_lemmatizer = WordNetLemmatizer()
    stop_words = set(stopwords.words('english'))

    # word tokenization
    word_tokens = nltk.word_tokenize(resume_text)

    # remove stop words and lemmatize
    filtered_sentence = [
        w for w in word_tokens if w not
        in stop_words and wordnet_lemmatizer.lemmatize(w)
        not in stop_words
    ]
    sent = nltk.pos_tag(filtered_sentence)

    # parse regex
    cp = nltk.RegexpParser('P: {<NNP>+}')
    cs = cp.parse(sent)

    # for i in cs.subtrees(filter=lambda x: x.label() == 'P'):
    #     print(i)

    test = []

    for vp in list(
        cs.subtrees(filter=lambda x: x.label() == 'P')
    ):
        test.append(" ".join([
            i[0] for i in vp.leaves()
            if len(vp.leaves()) >= 2])
        )

    # Search the word 'experience' in the chunk and
    # then print out the text after it
    experience = [
        x[x.lower().index('experience') + 10:]
        for i, x in enumerate(test)
        if x and 'experience' in x.lower()
    ]
    return experience


def get_total_experience(experience_list):
    '''
    Wrapper function to extract total months of experience from a resume
    :param experience_list: list of experience text extracted
    :return: total months of experience
    '''
    exp_ = []
    for line in experience_list:
        experience = re.search(
            r'(?P<fmonth>\w+.\d+)\s*(\D|to)\s*(?P<smonth>\w+.\d+|present)',
            line,
            re.I
        )
        if experience:
            exp_.append(experience.groups())
    total_exp = sum(
        [get_number_of_months_from_dates(i[0], i[2]) for i in exp_]
    )
    total_experience_in_months = total_exp
    return total_experience_in_months


def get_number_of_months_from_dates(date1, date2):
    '''
    Helper function to extract total months of experience from a resume
    :param date1: Starting date
    :param date2: Ending date
    :return: months of experience from date1 to date2
    '''
    if date2.lower() == 'present':
        date2 = datetime.now().strftime('%b %Y')
    try:
        if len(date1.split()[0]) > 3:
            date1 = date1.split()
            date1 = date1[0][:3] + ' ' + date1[1]
        if len(date2.split()[0]) > 3:
            date2 = date2.split()
            date2 = date2[0][:3] + ' ' + date2[1]
    except IndexError:
        return 0
    try:
        date1 = datetime.strptime(str(date1), '%b %Y')
        date2 = datetime.strptime(str(date2), '%b %Y')
        months_of_experience = relativedelta.relativedelta(date2, date1)
        months_of_experience = (months_of_experience.years
                                * 12 + months_of_experience.months)
    except ValueError:
        return 0
    return months_of_experience
