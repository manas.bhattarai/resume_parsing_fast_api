import uvicorn
from fastapi import FastAPI
from fastapi import FastAPI, File, UploadFile
from extract_resume_info import extract_resume_info
from extract_text import extract_text_from_resume
from pydantic import BaseModel

#define fastapi
app = FastAPI()

@app.post("/upload")
async def upload_file(file: UploadFile = File()):
    return {"Filename": file.filename} 
# @app.get("/")
# async def root():
#     return {"message": "Hello World"}
